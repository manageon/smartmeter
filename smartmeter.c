#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/types.h>
#include <fcntl.h>
#include <modbus/modbus.h>
#include <errno.h>
#include "cJSON.h"

#define CONFIG_FILENAME			"/mnt/config.json"
#define DEVICE_FILENAME 		"/dev/eintdev"
#define DISP_SEC_60 			60 
#define DISP_SEC_1 			1
#define SERVER_ID                       1
#define MODBUSTCP_PORTNUM               502
#define UT_BITS_NB                      0       //Number of DO
#define UT_INPUT_BITS_NB                0       //Number of DI
#define UT_INPUT_REGISTERS_NB           0       //Number of AI
#define UT_REGISTERS_NB                 28       //Number of AO

#define UT_BITS_ADDRESS                 0//1    //DO address
#define UT_INPUT_BITS_ADDRESS           0//2    //DI address
#define UT_INPUT_REGISTERS_ADDRESS      0//3    //AI address
#define UT_REGISTERS_ADDRESS            0//4    //AO address

/*
#define MAX(a,b) \
({ __typeof__ (a) _a = (a); \
__typeof__ (b) _b = (b); \
_a > _b ? _a : _b; })
  
#define MIN(a,b) \
({ __typeof__ (a) _a = (a); \
__typeof__ (b) _b = (b); \
_a < _b ? _a : _b; })
*/

enum {
	PULSECOUNT=0, 	//0
	PREVCOUNT,	//1
	CURRENTTIME,	//2
	RESETBEGIN,	//3
	COUNTSUM,	//4
	TARGETCOUNT,	//5
	RELAY1,		//6
	RELAY2,		//7
	PTRATIO,	//8
	CTRATIO,	//9
	PULSEPERKW,	//10
	TARGETPOWER,	//11
	FOREPOWER,	//12
	CURRENTPOWER,	//13
	REFPOWER,	//14
	LASTPOWER,	//15
	ADR_TARGET,	//16
	YEAR,		//17
	MONTH,		//18
	DAY,		//19
	HOUR,		//20
	MINUTE,		//21
	SECOND,		//22
	PULSECOUNT5,	//23
	PREVCOUNT5,	//24
	CURRENTTIME5,	//25
	CURRENTPOWER5,	//26
	HISTORYVIEWMODE //27		
};
const uint8_t UT_INPUT_BITS_TAB[] = { 0b11111110 };
const uint16_t UT_INPUT_REGISTERS_TAB[] = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8};
uint16_t UT_REGISTERS_TAB[UT_REGISTERS_NB] = {
	0, //PULSECOUNT=0, 	//0
	0, //PREVCOUNT,	//1
	0, //CURRENTTIME,	//2
	0, //RESETBEGIN,	//3
	0, //COUNTSUM,	//4
	0, //TARGETCOUNT,	//5
	0, //RELAY1,		//6
	0, //RELAY2,		//7
	4, //PTRATIO,	//8
	4, //CTRATIO,	//9
	1000, //PULSEPERKW,	//10
	0, //TARGETPOWER,	//11
	0, //FOREPOWER,	//12
	0, //CURRENTPOWER,	//13
	0, //REFPOWER,	//14
	0, //LASTPOWER,	//15
	0, //ADR_TARGET,	//16
	0, //YEAR,		//17
	0, //MONTH,		//18
	0, //DAY,		//19
	0, //HOUR,		//20
	0, //MINUTE,		//21
	0, //SECOND,		//22
	0, //PULSECOUNT5,	//23
	0, //PREVCOUNT5,	//24
	0, //CURRENTTIME5,	//25
	0, //CURRENTPOWER5,	//26
	0, //HISTORYVIEWMODE //27		
};


pthread_mutex_t mutx;
void *thr_readpulse(void *arg);
void *thr_modbus(void *arg);
void *thr_modbus_service(void *arg);
void sa_int_handler(int signum);
void tmr_update_handler (int signum);
void tmr_update_settimer(unsigned int sec);
sig_atomic_t tmr_atomic_value =0;
unsigned long gUpdateSec = 0;
unsigned long gPulseCount = 0;
unsigned long gFlagExit = 0, gFlagEoi=0, gFlagUpdate =0;
modbus_t *ctx;
modbus_mapping_t *mb_mapping;

int main(int argc, char *argv[])
{
	struct sigaction sa;
	//thread variables
	pthread_t tid_readpulse, tid_modbus;
	FILE *fp;
	long len;
	char *conf_data;
	cJSON *json;

	fp = fopen(CONFIG_FILENAME, "rb");
	if(NULL == fp) {
		perror("fopen()");
		exit(EXIT_FAILURE);
	}
	fseek(fp, 0, SEEK_END);
	len = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	conf_data = (char *)malloc(len+1);
	if(NULL == conf_data){
		perror("malloc()");
		fclose(fp);
		exit(EXIT_FAILURE);
	}
	fread(conf_data, 1, len, fp);
	fclose(fp);
	json = cJSON_Parse(conf_data);
	if(!json){
		printf("Error before: [%s]\n", cJSON_GetErrorPtr());
		free(conf_data);
		exit(EXIT_FAILURE);
	}
	UT_REGISTERS_TAB[PTRATIO] = 	cJSON_GetObjectItem(json, "pt-ratio")->valueint;
	UT_REGISTERS_TAB[CTRATIO] = 	cJSON_GetObjectItem(json, "ct-ratio")->valueint;
	UT_REGISTERS_TAB[PULSEPERKW] = 	MAX(cJSON_GetObjectItem(json, "pulse-kwh")->valueint, 1);
	/* Create EOI timer */
	gUpdateSec = cJSON_GetObjectItem(json, "disp-sec")->valueint;

	tmr_update_settimer(DISP_SEC_1);
	cJSON_Delete(json);
	free(conf_data);

/*	if(argc != 5){
		printf("Usage: %s <update-every-sec> <pt-r> <ct-r> <pulse-r>\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	UT_REGISTERS_TAB[PTRATIO] = 	atoi(argv[2]);
	UT_REGISTERS_TAB[CTRATIO] = 	atoi(argv[3]);
	UT_REGISTERS_TAB[PULSEPERKW] = 	MAX(atoi(argv[4]), 1);
*/
	/* Mutex initialize */
	pthread_mutex_init(&mutx, NULL);
	/* Create working threads */
	pthread_create(&tid_readpulse, NULL, thr_readpulse, NULL);
	pthread_detach(tid_readpulse);
	pthread_create(&tid_modbus, NULL, thr_modbus, NULL);
	pthread_detach(tid_modbus);

	memset(&sa, 0, sizeof(sa));
	sa.sa_handler = &sa_int_handler;
	sigaction(SIGINT, &sa, NULL);

	sleep(2);

	/* Main Loop */
	while(1){
		pthread_mutex_lock(&mutx);
		printf("Pulse: %ld\n", gPulseCount);
		pthread_mutex_unlock(&mutx);
		if(gFlagExit)
			break;
		sleep(1);
	}

	printf("Program Exit...\n");

	return 0;
}

void tmr_update_settimer(unsigned int sec)
{
	struct sigaction sa;
	struct itimerval timer;
	/* Install timer_handler as the signal handler for SIGVTALRM. */
        memset (&sa, 0, sizeof (sa));
        sa.sa_handler = &tmr_update_handler;
        sigaction (SIGALRM, &sa, NULL);
        /* Configure the timer to expire after xxx msec... */
        timer.it_value.tv_sec = sec;
        timer.it_value.tv_usec = 0;
        /* ... and every xxx msec after that. */
        timer.it_interval.tv_sec = sec;
        timer.it_interval.tv_usec = 0;
        /* Start a virtual timer. It cnt_eois down whenever this process is executing. */
        setitimer (ITIMER_REAL, &timer, NULL);
}

void sa_int_handler (int signum)
{
	gFlagExit = 1;
}

void tmr_update_handler (int signum)
{
	static int cnt_eoi = 1;
	static int cnt_update = 1;
//Every 1 sec
	cnt_eoi++;
	cnt_update++;

	if(cnt_update > gUpdateSec){
		gFlagUpdate =1;
		cnt_update = 1;
	}

	if(cnt_eoi > (5*60)) //Every 15 minutes, EOI
	//if(cnt_eoi > (15*60)) //Every 15 minutes, EOI
	{
		cnt_eoi = 1;
		gFlagEoi = 1;
		printf("[Timer] 5 Minutes Timer expired\n");
	}
	mb_mapping->tab_registers[CURRENTTIME] = cnt_eoi;
}

void *thr_readpulse(void *arg)
{
	int dev;
	unsigned long rBuff, wBuff;
	volatile double calc1, calc2;
	
	dev = open(DEVICE_FILENAME, O_RDWR|O_NDELAY);
	if(dev < 0){
		return  NULL;
	}else{
		while(1){
			if(gFlagEoi){
				wBuff = 0;
				write(dev, &wBuff, sizeof(wBuff));
				gFlagEoi = 0;
				
				//prev power consumption kWh
				mb_mapping->tab_registers[LASTPOWER] = mb_mapping->tab_registers[ADR_TARGET];
				//prev counter
				mb_mapping->tab_registers[PREVCOUNT] = mb_mapping->tab_registers[PULSECOUNT];
			}
			rBuff = 0;
			if(read(dev, &rBuff, sizeof(rBuff)) == sizeof(rBuff)){
				gPulseCount = rBuff;
				pthread_mutex_lock(&mutx);
				mb_mapping->tab_registers[PULSECOUNT] = gPulseCount;
				//mb_mapping->tab_registers[PULSECOUNT5] = gPulseCount;
				//Calculate Current Power
				if(gFlagUpdate){
					gFlagUpdate = 0;

					calc1 = mb_mapping->tab_registers[PTRATIO];
					calc2 = mb_mapping->tab_registers[CTRATIO];
					calc1 *= calc2;
					calc2 = mb_mapping->tab_registers[PULSEPERKW];
					
					if(0 == calc2){
						calc2 = 1000;
						mb_mapping->tab_registers[PULSEPERKW] = 1000;
					}
					calc1 /= calc2;
					calc2 = gPulseCount;
					calc1 *= calc2;
					//current kWh
					mb_mapping->tab_registers[ADR_TARGET] = (uint16_t)calc1;
					printf("power consumption: %f\n", calc1);

					calc1 *= (60.0 * 60.0);
					calc2 = mb_mapping->tab_registers[CURRENTTIME];
					calc2 -= 1;
					if(0 == calc2){
						calc2 = 1;
					}
					calc1 /= calc2;
					mb_mapping->tab_registers[CURRENTPOWER] = (uint16_t)calc1;
					printf("current power: %f\n", calc1);
				}
				pthread_mutex_unlock(&mutx);
			}
			if(gFlagExit){
				break;
			}

			sleep(DISP_SEC_1);	
		}
		close(dev);
	}
}
void *thr_modbus(void *arg)
{
	//modbus variables
	int i;
	int socket_server;
	int *socket_client;
	pthread_t tid_modbus_service;

	/* make modbus service */
	ctx = modbus_new_tcp("127.0.0.1", MODBUSTCP_PORTNUM);
	if(ctx == NULL){
		perror("modbus_new_tcp()");
		gFlagExit =1;
		return;
	}

	modbus_set_debug(ctx, FALSE);

	//get lock for shared data ==> mb_mapping
	pthread_mutex_lock(&mutx);
	mb_mapping = modbus_mapping_new(
		UT_BITS_ADDRESS                 +UT_BITS_NB,
		UT_INPUT_BITS_ADDRESS           +UT_INPUT_BITS_NB,
		UT_REGISTERS_ADDRESS            +UT_REGISTERS_NB,
		UT_INPUT_REGISTERS_ADDRESS      +UT_INPUT_REGISTERS_NB);
	pthread_mutex_unlock(&mutx);

	if(mb_mapping == NULL){
		perror("modbus_mapping_new()");
		modbus_free(ctx);
		gFlagExit =1;
		return;
	}

	/** INIT. INPUT STATUS **/
	pthread_mutex_lock(&mutx);
	modbus_set_bits_from_bytes(mb_mapping->tab_input_bits,
		UT_INPUT_BITS_ADDRESS,
		UT_INPUT_BITS_NB,
		UT_INPUT_BITS_TAB);
	/** INIT. INPUT REGISTERS **/
	for (i=0; i < UT_INPUT_REGISTERS_NB; i++) {
		mb_mapping->tab_input_registers[i] =
		UT_INPUT_REGISTERS_TAB[i];;
	}
	/** INIT. HOLDING REGISTERS **/
	for (i=0; i < UT_REGISTERS_NB; i++) {
		mb_mapping->tab_registers[i] =
		UT_REGISTERS_TAB[i];;
	}
	pthread_mutex_unlock(&mutx);
	socket_server = modbus_tcp_listen(ctx, 1);
	/* Work Loop */
	while(1){
		socket_client = malloc(sizeof(int));
		*socket_client = modbus_tcp_accept(ctx, &socket_server);
		if(*socket_client > 0)
		{
			pthread_create(&tid_modbus_service, NULL,thr_modbus_service, (void *)socket_client);
			pthread_detach(tid_modbus_service);
		}

		if(gFlagExit)
			break;
	}
	close(socket_server);
	modbus_mapping_free(mb_mapping);
	modbus_free(ctx);
}

void *thr_modbus_service(void *arg)
{
	int *socket_client = (int *)arg;
	uint8_t *query;
	int rc;
	uint8_t brg_func;
	uint16_t brg_addr, brg_quant;
	int header_length;

	header_length = modbus_get_header_length(ctx);

	query = malloc(MODBUS_TCP_MAX_ADU_LENGTH);
	if(query == NULL){
		close(*socket_client);
		free(socket_client);
		perror("malloc()");
		return NULL;
	}

	rc = modbus_receive(ctx, query);
	if(rc == -1){
		close(*socket_client);
		free(socket_client);
		free(query);
		perror("modbus_receive()");
		return NULL;
	}
		
	 /* Check Function Code, Address and Quantity */
	brg_func = query[header_length];
	brg_addr = MODBUS_GET_INT16_FROM_INT8(query, header_length + 1);
	brg_quant = MODBUS_GET_INT16_FROM_INT8(query, header_length + 3);
//	printf("FUNC CODE: %2d \nADDRESS: %5d \nQuantity: %5d \n", brg_func, brg_addr, brg_quant);

	pthread_mutex_lock(&mutx);
	rc = modbus_reply(ctx, query, rc, mb_mapping);
	pthread_mutex_unlock(&mutx);

	/* finish procedure */
	close(*socket_client);
	free(socket_client);
	free(query);
	return NULL;
}
