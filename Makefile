SRCS    = smartmeter.c cJSON.c
OBJS    = $(SRCS:.c=.o)
PROG    = smartmeter
#INSTALL_ROOT = /var/nfs/usr/local

LDFLAGS = -lpthread -lmodbus -lm

#ifeq ($(ARCH),armv6)
#CFLAGS  = -g -O0 -DONARMBOARD
#else
CFLAGS  = -g -O0 -D_REENTRANT
# -I/usr/local/arm-2009q1/arm-none-linux-gnueabi/libc/include
#endif


$(PROG): $(OBJS)
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

$(OBJS): cJSON.h

clean:
	rm -f $(OBJS) $(PROG)

#install: $(PROG)
#	sudo cp $(PROG) $(INSTALL_ROOT)/bin
